# thematous.module

A facility to maintain themes as nodes of type 'thematous':

- Knows about autosite
- Knows about sub-themes
- Allows to restrict themes to specific parent themes
- Sub-themes may define just a stylesheet or stylesheet, TAL templates and template.php
 